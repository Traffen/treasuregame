'use strict';


var util = require('util')

var userTemperatures = new Map();

var checkingInterval = process.env.CHECKING_INTERVAL || 1*20*1000
var refreshInterval = process.env.REFRESH_INTERVAL || 1*60*1000

var defaultTemp = 22.0;
var currentTemperature = defaultTemp;


function checkUserActivity(){
    console.log("Current connected users: ");
    if(userTemperatures.size == 0){
        console.log("No users are connected");
    }else{
        for(var entry of userTemperatures.entries()){
            var userName = entry[0];
            var value = entry[1];
            var temperature = value.temperature;
            var seenCounter = value.seenCounter;
            console.log("User: " + userName + ", temperature: " + temperature + ", seen counter: " + seenCounter)
        }
    }

    console.log("Current temperature: " + currentTemperature);

    
}

function refreshUserList(){
    for(var entry of userTemperatures.entries()){
        var userName = entry[0];
        var value = entry[1];
        var temperature = value.temperature;
        var seenCounter = value.seenCounter;
        seenCounter--;
        if(seenCounter < 1){
            //remove user from list
            userTemperatures.delete(userName);
            console.log("Removed user " + userName + " from entries");
            setTemperature();
        }else{
            //decrement seenCounter
            userTemperatures.set(userName, {temperature, seenCounter})
            
        }
    }

    
}

function setTemperature(){
    
    if(userTemperatures.size == 0){
        //no users connected, set default temperature (or turn off)
        console.log("No users connected. Setting temperature to default: " + defaultTemp)
        currentTemperature = defaultTemp;
    }else{
        var temperature = 0.0;

        for(var entry of userTemperatures.entries()){
            var value = entry[1];
            temperature = temperature + parseInt(value.temperature, 10);
        }
        temperature = temperature / userTemperatures.size
        currentTemperature = temperature;
        console.log("Setting temperature to: " + currentTemperature)

    }
}

//execute this function every 5 minutes
setInterval(checkUserActivity, checkingInterval);
setInterval(refreshUserList, refreshInterval)
//setInterval(setTemperature, setTemperatureInterval)

exports.sendTemperature = function(req, res){
    var userName = req.body.userName;
    var temperature = req.body.temperature;
    var seenCounter = 2;
    var response;
    if(userTemperatures.get(userName) == undefined){
        //user does not exist, create new user
        userTemperatures.set(userName, {temperature, seenCounter});
        console.log("Added user: " + userName + " with temperature: "+ temperature); 
        response = "Added new user";
        setTemperature();
    }else{
        //user exists, update its seenCounter
        userTemperatures.set(userName, {temperature, seenCounter})
        console.log("Refreshed user " + userName + " counter");
        response = "Refreshed user"
    }
    res.send(response);
    
}

exports.get_script = function (req, res) {
  console.log("get AC script");
    var fs = require('fs');
    fs.readFile('Script/ACScript.bsh', 'UTF-8', function (err, data) {
        if (err) res.send(err);
        // console.log(data);
        var script = data;
        console.log("AC script sent");
        res.send(script);
      });
}


